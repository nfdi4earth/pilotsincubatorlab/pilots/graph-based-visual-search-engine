# VESA  (Visualisation Enable Search Application)

<p align="center">
  <img src="/docs/assets/Hero.png" alt="Hero image">
</p>

Repository for the Visualisation Enabled Search Application. It is a visual exploraion and search tool that assist users in nativating through the graph in an intuitive way. Different visualizations assist in finding information in different dimensions. For example: Map &rarr; Spatial Context, Line Charts &rarr; temporal context, Network Diagrams or graphs &rarr; interrelations, Word Cloud &rarr; Thematic context, etc.  These dimensions, for e.g are, dataset's locations &rarr; Map, measurement datas &rarr; temporal charts, a world cloud to show different context of datasets.

<p align="center">
  <img src="./docs/assets/readme.png" alt="Workflow">
</p>

[[_TOC_]]

## ✨Features
You can find a list of the latest changes in the [CHANGELOG]()

## 📝Prerequisites

Before you begin, ensure you have the following software installed on your machine:

- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/) (Node.js package manager)

### Installation

If you don't have Node.js and npm installed, you can download and install them from the official websites:

- **Node.js:** Visit [Node.js official website](https://nodejs.org/) and follow the installation instructions for your operating system.

- **npm:** npm is included with Node.js installation. After installing Node.js, you'll have npm available on your command line.

To verify that Node.js and npm are installed, open a terminal or command prompt and run the following commands:

```bash
node -v
npm -v
```


## 🚀Quick Start


Follow these steps to quickly get started with the project:

1. **Clone the Repository:**

    ```bash
    git clone https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots/graph-based-visual-search-engine.git
    ```

2. **Navigate to the Project Directory:**

    ```bash
    cd code
    ```

3. **Install Frontend Dependencies:**

    ```bash
    cd FRONTEND
    npm install
    ```

4. **Install Backend Dependencies:**

    ```bash
    cd ../BACKEND  # Navigate back to the project root directory
    npm install
    ```


## 🌐Running the Application

- **Run Frontend:**

    ```bash
    cd Frontend
    npm start
    ```

    This will start the frontend development server.

- **Run Backend:**

    ```bash
    cd ../Backend  # Navigate back to the project root directory
    npm run dev
    ```

    This will start the backend server in development mode.

Now you have successfully cloned the repository, navigated into the project directory, and started both the frontend and backend components.

> 💡 **TIP**
> Make sure to follow any additional instructions provided in the project documentation for a complete setup.


## 🙌How to Contribute

Whenever you encounter a 🐛 bug or have 🎉 feature request, report this via Gitlab issues.

We are happy to receive contributions to VESA in the form of pull requests via Gitlab. Feel free to fork the repository, implement your changes and create a merge request to the develop branch. There is a [forking guide](/CONTRIBUTING.md#forking) available to get you started!

> 💡 **Further info about Contribution**
> Please refer [CONTRIBUTING.md](/CONTRIBUTING.md)

## 📖Further Documentsion

An overview about which documentation can be found where. For example the [docs](/docs/) directory

## 🙋Contributor Covenant Code of Conduct

### Our Pledge
In the interest of an open and wrlcoming environment, we as contributors and maintainers pledge to making participation in our project and out community a harrassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

Read further about the Code of Conduct [here](/CODE_OF_CONDUCT.md).

## 📄REUSE Compliance

Either set up an automatic system to add REUSE compliant licensing to each file or add then manually for new files. Make sute to add checks to the code review to verify REUSE compliance.